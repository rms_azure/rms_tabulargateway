﻿using RmsGateway.DataModel;
using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Windows.Forms;
using RmsGateway.ShareModel;
using System.Collections.Generic;
using System.ComponentModel;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Test test;
        ISignalsContract channel;

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (test == null)
                test = new Test(dataGridView1);

            string address = "net.pipe://localhost/RmsGateway/Signals";
            NetNamedPipeBinding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.Transport);
            EndpointAddress ep = new EndpointAddress(address);
            channel = new DuplexChannelFactory<ISignalsContract>(test, binding, ep).CreateChannel();
            channel.SubscribeSignals("test");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            channel.UnsubscribeSignals("test");
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            test.Filter(textBox1.Text);
        }
    }


    [DataContract]
    public class Test : ISignalsSubscriber
    {
        DataGridView grid;

        Dictionary<string, Signal> dataInMemory = new Dictionary<string, Signal>();
        BindingList<Signal> bindableSignals = new BindingList<Signal>();

        public Test(DataGridView a)
        {
            grid = a;

            BindingSource bs = new BindingSource(bindableSignals, null);
            grid.DataSource = bs;
        }

        public void Filter(string text)
        {
            if (text != null && text != "")
            {
                BindingList<Signal> filtered = new BindingList<Signal>(); // .Where(p => p.tag.Contains(text)).ToList());
                foreach (var s in bindableSignals)
                    if (s.Tag.Contains(text))
                        filtered.Add(s);

                BindingSource bs = new BindingSource(filtered, null);
                grid.DataSource = bs;
            }
            else
            {
                BindingSource bs = new BindingSource(bindableSignals, null);
                grid.DataSource = bs;
            }
        }

        public void NewAnalogSignal(AnalogSignal signal)
        {
            ProcessMessage(signal);
        }

        public void NewDigitalSignal(DigitalSignal signal)
        {
            ProcessMessage(signal);
        }

        public void NewStringSignal(StringSignal signal)
        {
            // textBox.AppendText(String.Format("{0} // {1} // {2}\n", signal.Tag, signal.Value, signal.Quality == SignalQuality.Good ? "Good" : "Bad"));

            /*
            if (!dataInMemory.ContainsKey(signal.Tag))
            {
                dataInMemory.Add(signal.Tag, signal);
                bindableSignals.Add(signal);
            }
            else
            {
                var index = bindableSignals.IndexOf(signal);
                var existing = (StringSignal) bindableSignals[index];
                existing.FieldTime = signal.FieldTime;
                existing.Value = signal.Value;
                existing.Quality = signal.Quality;

                dataInMemory[signal.Tag] = existing;
            }
            */


            ProcessMessage(signal);
        }


        private void ProcessMessage(RmsGateway.ShareModel.Signal signal)
        {
            bool foundSignal = false;

            foreach (Signal s in bindableSignals)
            {
                if (s.TI == signal.Handle)
                {
                    if (signal.Type == RmsGateway.ShareModel.SignalType.Analog)
                        s.Value = ((AnalogSignal)signal).Value.ToString();
                    else if (signal.Type == RmsGateway.ShareModel.SignalType.Point)
                        s.Value = ((DigitalSignal)signal).Value.ToString();
                    else
                        s.Value = ((StringSignal)signal).Value.ToString();


                    if (signal.Quality == RmsGateway.ShareModel.SignalQuality.Good) s.Quality = "Good";
                    else s.Quality = "Bad";

                    s.Timestamp = signal.FieldTime.ToLocalTime().ToString();
                    foundSignal = true;
                    break;
                }
            }

            if (!foundSignal)
            {
                Signal newSignal = new Signal
                {
                    Tag = signal.Tag
                };

                if (signal.Type == RmsGateway.ShareModel.SignalType.Analog)
                {
                    newSignal.Value = ((AnalogSignal)signal).Value.ToString();
                    newSignal.Type = "Analog";
                }
                else if (signal.Type == RmsGateway.ShareModel.SignalType.Point)
                {
                    newSignal.Value = ((DigitalSignal)signal).Value.ToString();
                    newSignal.Type = "Digital";
                }
                else
                {
                    newSignal.Value = ((StringSignal)signal).Value.ToString();
                    newSignal.Type = "String";
                }

                newSignal.TI = signal.Handle;

                if (signal.Quality == RmsGateway.ShareModel.SignalQuality.Good)
                    newSignal.Quality = "Good";
                else newSignal.Quality = "Bad";

                newSignal.Timestamp = signal.FieldTime.ToLocalTime().ToString();

                bindableSignals.Add(newSignal);
                dataInMemory.Add(signal.Tag, newSignal);
            }
        }


        public class Signal : INotifyPropertyChanged
        {
            private string _V;
            private string _q;
            private string _ts;

            public uint TI { get; set; }

            public string Tag { get; set; }

            public string Type { get; set; }

            public string Value
            {
                get { return _V; }
                set { _V = value; NotifyPropertyChanged("V"); }
            }

            public string Quality
            {
                get { return _q; }
                set { _q = value; NotifyPropertyChanged("q"); }
            }

            public string Timestamp
            {
                get { return _ts; }
                set { _ts = value; NotifyPropertyChanged("ts"); }
            }

            private void NotifyPropertyChanged(string v)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }
    }
}
